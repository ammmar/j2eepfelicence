package cnss.local.service;

import java.util.List;

import cnss.local.entity.Etatreclamation;
public interface EtatService 
{
List<Etatreclamation>getAllEtat();
void save(Etatreclamation er);
void update (Etatreclamation er);
}
