package cnss.local.service;

import java.util.*;

import cnss.local.entity.*;
public interface ReclamationService {
	List<Reclamation>getAllreclamation();
	Reclamation getReclamationById(long idR);
	void update (Reclamation r);
	void saveEtat(Etatreclamation er);
	void SaveReponse(Reponse rep);
}
