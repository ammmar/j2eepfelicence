package cnss.local.service;

import java.util.List;

import cnss.local.entity.Client;
public interface ClientService {
	List<Client>getAllClient();
	void saveOrUpdateClient(Client C);
	void saveClient(Client C);
	void updateClient(Client c);
	public boolean ExisteClient(String password,long matricule);
	 List<Client> Rechercheby2Properties(String propertyName1,Object value1,String propertyName2,Object value2);	
	 List<Client> Rechercheby1Properties(String propertyName1,Object value1);	
}
