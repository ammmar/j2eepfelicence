package cnss.local.service;

import java.util.List;

import cnss.local.entity.Administrateur;


public interface AdminService {
	List<Administrateur>getAllAdmin();
	public Administrateur getAdmin(String Login, String mp);
	void update(Administrateur a);
	 List<Administrateur> Rechercheby1Properties(String propertyName1,Object value1);	
	 List<Administrateur> Rechercheby2Properties(String propertyName1,Object value1,String propertyName2,Object value2);	
}
