package cnss.local.dao;



import cnss.local.entity.Client;

public interface ClientDao extends GenericDao<Client, Long>  {
	
}
