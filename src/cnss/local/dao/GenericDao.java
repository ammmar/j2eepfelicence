package cnss.local.dao;


import java.util.List;
	 
	public interface GenericDao <E, PK extends java.io.Serializable>
	{
	    public PK save(E newInstance);
	    public void update(E transientObject);
	    public void saveOrUpdate(E transientObject);
	    public void delete(E persistentObject);
	    public E findById(PK id);
	    public List<E> findAll();
	    public List<E> findAllBy1Properties(String propertyName1, Object value1);	    
	    public List<E> findAllBy2Properties(String propertyName1,Object value1,String propertyName2,Object value2);
	}

