package cnss.local.dao;

import cnss.local.entity.Reclamation;

public interface ReclamationDao extends GenericDao<Reclamation, Long> {
	
}
