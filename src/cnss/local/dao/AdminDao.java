package cnss.local.dao;

import cnss.local.entity.Administrateur;

public interface AdminDao  extends GenericDao<Administrateur, Long> {
	
}
