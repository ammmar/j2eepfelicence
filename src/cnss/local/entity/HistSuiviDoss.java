package cnss.local.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the HIST_SUIVI_DOSS database table.
 * 
 */
@Entity
@Table(name="HIST_SUIVI_DOSS")
@NamedQuery(name="HistSuiviDoss.findAll", query="SELECT h FROM HistSuiviDoss h")
public class HistSuiviDoss implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(unique=true, nullable=false, precision=10)
	private long id;

	@Column(name="CODE_CPT", nullable=false, precision=22)
	private BigDecimal codeCpt;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_E")
	private Date dateE;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_R")
	private Date dateR;

	@Column(name="SUI_ORD", nullable=false, precision=22)
	private BigDecimal suiOrd;

	@Column(name="UTL_MATR", nullable=false, precision=22)
	private BigDecimal utlMatr;
	
	@ManyToOne
	@JoinColumn(name="NUM_DOSS", nullable=false)
	private Client client;

	@ManyToOne
	@JoinColumn(name="STR_CODE_R", nullable=false)
	private Structure structure1;

	@ManyToOne
	@JoinColumn(name="STR_CODE_E", nullable=false)
	private Structure structure2;

	@ManyToOne
	@JoinColumn(name="TRT_CODE", nullable=false)
	private Traitement traitement;

	public HistSuiviDoss() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getCodeCpt() {
		return this.codeCpt;
	}

	public void setCodeCpt(BigDecimal codeCpt) {
		this.codeCpt = codeCpt;
	}

	public Date getDateE() {
		return this.dateE;
	}

	public void setDateE(Date dateE) {
		this.dateE = dateE;
	}

	public Date getDateR() {
		return this.dateR;
	}

	public void setDateR(Date dateR) {
		this.dateR = dateR;
	}

	public BigDecimal getSuiOrd() {
		return this.suiOrd;
	}

	public void setSuiOrd(BigDecimal suiOrd) {
		this.suiOrd = suiOrd;
	}

	public BigDecimal getUtlMatr() {
		return this.utlMatr;
	}

	public void setUtlMatr(BigDecimal utlMatr) {
		this.utlMatr = utlMatr;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Structure getStructure1() {
		return this.structure1;
	}

	public void setStructure1(Structure structure1) {
		this.structure1 = structure1;
	}

	public Structure getStructure2() {
		return this.structure2;
	}

	public void setStructure2(Structure structure2) {
		this.structure2 = structure2;
	}

	public Traitement getTraitement() {
		return this.traitement;
	}

	public void setTraitement(Traitement traitement) {
		this.traitement = traitement;
	}

	@Override
	public String toString() {
		return "HistSuiviDoss [id=" + id + ", codeCpt=" + codeCpt + ", dateE=" + dateE + ", dateR=" + dateR
				+ ", suiOrd=" + suiOrd + ", utlMatr=" + utlMatr + ", client=" + client + ", structure1=" + structure1
				+ ", structure2=" + structure2 + ", traitement=" + traitement + "]";
	}

	
}