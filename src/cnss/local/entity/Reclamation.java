package cnss.local.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


/**
 * The persistent class for the RECLAMATION database table.
 * 
 */
@Entity
@Table(name="RECLAMATION")
@NamedQuery(name="Reclamation.findAll", query="SELECT r FROM Reclamation r")
public class Reclamation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RECLAMATION_IDRECLAMATION_GENERATOR", sequenceName="ADMIN_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RECLAMATION_IDRECLAMATION_GENERATOR")
	@Column(unique=true, nullable=false, precision=10)
	private long idreclamation;

	@Column(nullable=false, precision=22)
	private BigDecimal cle;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date daterecu;

	@Column(nullable=false, length=3000)
	private String descrip;
	
	@OneToMany(mappedBy="reclamation",fetch=FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	private List<Etatreclamation> etatreclamations;
	
	@ManyToOne
	@JoinColumn(name="MATRICULE", nullable=false)
	private Client client;

	@ManyToOne
	@JoinColumn(name="IDTYPE", nullable=false)
	private Typereclamation typereclamation;

	@OneToMany(mappedBy="reclamation",fetch=FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	private List<Reponse> reponses;
	
	@Transient
	private String lastDate;

	@Transient
	private String lastDateEtatValue;
	

	public Reclamation() {
	}

	public long getIdreclamation() {
		return this.idreclamation;
	}

	public void setIdreclamation(long idreclamation) {
		this.idreclamation = idreclamation;
	}

	

	public String getLastDateEtatValue() {
		return lastDateEtatValue;
	}

	public void setLastDateEtatValue(String lastDateEtatValue) {
		this.lastDateEtatValue = lastDateEtatValue;
	}

	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}

	public BigDecimal getCle() {
		return this.cle;
	}

	public void setCle(BigDecimal cle) {
		this.cle = cle;
	}

	public Date getDaterecu() {
		return this.daterecu;
	}

	public void setDaterecu(Date daterecu) {
		this.daterecu = daterecu;
	}

	public String getDescrip() {
		return this.descrip;
	}

	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}

	public List<Etatreclamation> getEtatreclamations() {
		return this.etatreclamations;
	}

	public void setEtatreclamations(List<Etatreclamation> etatreclamations) {
		this.etatreclamations = etatreclamations;
	}

	public Etatreclamation addEtatreclamation(Etatreclamation etatreclamation) {
		getEtatreclamations().add(etatreclamation);
		etatreclamation.setReclamation(this);

		return etatreclamation;
	}

	public Etatreclamation removeEtatreclamation(Etatreclamation etatreclamation) {
		getEtatreclamations().remove(etatreclamation);
		etatreclamation.setReclamation(null);

		return etatreclamation;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Typereclamation getTypereclamation() {
		return this.typereclamation;
	}

	public void setTypereclamation(Typereclamation typereclamation) {
		this.typereclamation = typereclamation;
	}

	public List<Reponse> getReponses() {
		return this.reponses;
	}

	public void setReponses(List<Reponse> reponses) {
		this.reponses = reponses;
	}

	public Reponse addRepons(Reponse repons) {
		getReponses().add(repons);
		repons.setReclamation(this);
		return repons;
	}

	public Reponse removeRepons(Reponse repons) {
		getReponses().remove(repons);
		repons.setReclamation(null);

		return repons;
	}

	@Override
	public String toString() {
		return "Reclamation [idreclamation=" + idreclamation + ", daterecu=" + daterecu + ", descrip=" + descrip + "]";
	}
	
}