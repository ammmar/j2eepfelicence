package cnss.local.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the "STRUCTURE" database table.
 * 
 */
@Entity
//@Table(name="\"STRUCTURE\"")
@NamedQuery(name="Structure.findAll", query="SELECT s FROM Structure s")
public class Structure implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STR_CODE", unique=true, nullable=false, precision=22)
	private long strCode;

	@Column(name="STR_LIB", length=100)
	private String strLib;

	@Column(name="STR_LIB_AR", length=100)
	private String strLibAr;
	@OneToMany(mappedBy="structure1")
	private List<HistSuiviDoss> histSuiviDosses1;

	@OneToMany(mappedBy="structure2")
	private List<HistSuiviDoss> histSuiviDosses2;

	public Structure() {
	}

	public long getStrCode() {
		return this.strCode;
	}

	public void setStrCode(long strCode) {
		this.strCode = strCode;
	}

	public String getStrLib() {
		return this.strLib;
	}

	public void setStrLib(String strLib) {
		this.strLib = strLib;
	}

	public String getStrLibAr() {
		return this.strLibAr;
	}

	public void setStrLibAr(String strLibAr) {
		this.strLibAr = strLibAr;
	}

	public List<HistSuiviDoss> getHistSuiviDosses1() {
		return this.histSuiviDosses1;
	}

	public void setHistSuiviDosses1(List<HistSuiviDoss> histSuiviDosses1) {
		this.histSuiviDosses1 = histSuiviDosses1;
	}

	public HistSuiviDoss addHistSuiviDosses1(HistSuiviDoss histSuiviDosses1) {
		getHistSuiviDosses1().add(histSuiviDosses1);
		histSuiviDosses1.setStructure1(this);

		return histSuiviDosses1;
	}

	public HistSuiviDoss removeHistSuiviDosses1(HistSuiviDoss histSuiviDosses1) {
		getHistSuiviDosses1().remove(histSuiviDosses1);
		histSuiviDosses1.setStructure1(null);

		return histSuiviDosses1;
	}

	public List<HistSuiviDoss> getHistSuiviDosses2() {
		return this.histSuiviDosses2;
	}

	public void setHistSuiviDosses2(List<HistSuiviDoss> histSuiviDosses2) {
		this.histSuiviDosses2 = histSuiviDosses2;
	}

	public HistSuiviDoss addHistSuiviDosses2(HistSuiviDoss histSuiviDosses2) {
		getHistSuiviDosses2().add(histSuiviDosses2);
		histSuiviDosses2.setStructure2(this);

		return histSuiviDosses2;
	}

	public HistSuiviDoss removeHistSuiviDosses2(HistSuiviDoss histSuiviDosses2) {
		getHistSuiviDosses2().remove(histSuiviDosses2);
		histSuiviDosses2.setStructure2(null);

		return histSuiviDosses2;
	}

	@Override
	public String toString() {
		return "Structure [strCode=" + strCode + ", strLib=" + strLib + ", strLibAr=" + strLibAr + ", histSuiviDosses1="
				+ histSuiviDosses1 + ", histSuiviDosses2=" + histSuiviDosses2 + "]";
	}

	

	
}