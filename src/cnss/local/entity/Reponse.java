package cnss.local.entity;

import java.io.Serializable;
import javax.persistence.*;



import java.util.Date;


/**
 * The persistent class for the REPONSE database table.
 * 
 */
@Entity
@Table(name="REPONSE")
@NamedQuery(name="Reponse.findAll", query="SELECT r FROM Reponse r")
public class Reponse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="REPONSE_IDREPONSE_GENERATOR", sequenceName="REPONSE_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPONSE_IDREPONSE_GENERATOR")
	@Column(unique=true, nullable=false, precision=10)
	private long idreponse;

	@Column(length=3000)
	private String contenurep;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date datereponse;

	@ManyToOne
	@JoinColumn(name="IDADMIN", nullable=false)
	private Administrateur administrateur;

	@ManyToOne
	@JoinColumn(name="IDRECLAMATION", nullable=false)
	private Reclamation reclamation;

	public Reponse() {
	}

	public long getIdreponse() {
		return this.idreponse;
	}

	public void setIdreponse(long idreponse) {
		this.idreponse = idreponse;
	}

	public String getContenurep() {
		return this.contenurep;
	}

	public void setContenurep(String contenurep) {
		this.contenurep = contenurep;
	}

	public Date getDatereponse() {
		return this.datereponse;
	}

	public void setDatereponse(Date datereponse) {
		this.datereponse = datereponse;
	}

	public Administrateur getAdministrateur() {
		return this.administrateur;
	}

	public void setAdministrateur(Administrateur administrateur) {
		this.administrateur = administrateur;
	}

	public Reclamation getReclamation() {
		return this.reclamation;
	}

	public void setReclamation(Reclamation reclamation) {
		this.reclamation = reclamation;
	}

	@Override
	public String toString() {
		return "Reponse [contenurep=" + contenurep + ", datereponse=" + datereponse + ", administrateur="
				+ administrateur + ", reclamation=" + reclamation + "]";
	}
	
	

}