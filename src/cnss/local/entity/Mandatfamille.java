package cnss.local.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the MANDATFAMILLE database table.
 * 
 */
@Entity
@Table(name="MANDATFAMILLE")
@NamedQuery(name="Mandatfamille.findAll", query="SELECT m FROM Mandatfamille m")
public class Mandatfamille implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, precision=10)
	private long id;

	@Column(nullable=false, length=11)
	private String annee;

	@Column(nullable=false, precision=22)
	private BigDecimal cle;

	@Column(name="DATE_MANDATEMENT", nullable=false, length=11)
	private String dateMandatement;

	@Column(name="MONTANT_AF", nullable=false, length=11)
	private String montantAf;

	@Column(name="MONTANT_SU", nullable=false, length=11)
	private String montantSu;

	@Column(name="NBRE_ENF_MANDATES", nullable=false, length=11)
	private String nbreEnfMandates;

	@Column(name="NBRE_ENF_SUSP", nullable=false, length=11)
	private String nbreEnfSusp;

	@Column(name="NUM_EMISSION", nullable=false, length=11)
	private String numEmission;

	@Column(nullable=false, length=11)
	private String tr;

	@Column(name="TYPE_PAIEMENT", nullable=false, length=11)
	private String typePaiement;

	@ManyToOne
	@JoinColumn(name="MATRICULE", nullable=false)
	private Client client;

	public Mandatfamille() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAnnee() {
		return this.annee;
	}

	public void setAnnee(String annee) {
		this.annee = annee;
	}

	public BigDecimal getCle() {
		return this.cle;
	}

	public void setCle(BigDecimal cle) {
		this.cle = cle;
	}

	public String getDateMandatement() {
		return this.dateMandatement;
	}

	public void setDateMandatement(String dateMandatement) {
		this.dateMandatement = dateMandatement;
	}

	public String getMontantAf() {
		return this.montantAf;
	}

	public void setMontantAf(String montantAf) {
		this.montantAf = montantAf;
	}

	public String getMontantSu() {
		return this.montantSu;
	}

	public void setMontantSu(String montantSu) {
		this.montantSu = montantSu;
	}

	public String getNbreEnfMandates() {
		return this.nbreEnfMandates;
	}

	public void setNbreEnfMandates(String nbreEnfMandates) {
		this.nbreEnfMandates = nbreEnfMandates;
	}

	public String getNbreEnfSusp() {
		return this.nbreEnfSusp;
	}

	public void setNbreEnfSusp(String nbreEnfSusp) {
		this.nbreEnfSusp = nbreEnfSusp;
	}

	public String getNumEmission() {
		return this.numEmission;
	}

	public void setNumEmission(String numEmission) {
		this.numEmission = numEmission;
	}

	public String getTr() {
		return this.tr;
	}

	public void setTr(String tr) {
		this.tr = tr;
	}

	public String getTypePaiement() {
		return this.typePaiement;
	}

	public void setTypePaiement(String typePaiement) {
		this.typePaiement = typePaiement;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@Override
	public String toString() {
		return "Mandatfamille [id=" + id + ", annee=" + annee + ", cle=" + cle + ", dateMandatement=" + dateMandatement
				+ ", montantAf=" + montantAf + ", montantSu=" + montantSu + ", nbreEnfMandates=" + nbreEnfMandates
				+ ", nbreEnfSusp=" + nbreEnfSusp + ", numEmission=" + numEmission + ", tr=" + tr + ", typePaiement="
				+ typePaiement + ", client=" + client + "]";
	}

	
}