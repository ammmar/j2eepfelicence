package cnss.local.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the CONSULTDROITALLOC database table.
 * 
 */
@Entity
@Table(name="CONSULTDROITALLOC")
@NamedQuery(name="Consultdroitalloc.findAll", query="SELECT c FROM Consultdroitalloc c")
public class Consultdroitalloc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, precision=10)
	private long id;

	@Column(nullable=false, precision=22)
	private BigDecimal cle;

	@Column(name="CODE_SUSP", nullable=false, length=11)
	private String codeSusp;

	@Column(name="DATE_DEPART_DROITS", nullable=false, length=11)
	private String dateDepartDroits;

	@Column(name="DATE_FIN_DROITS", nullable=false, length=11)
	private String dateFinDroits;

	@Column(name="DATE_NAISSANCE", nullable=false, length=11)
	private String dateNaissance;

	@Column(name="DROIT_AF", nullable=false, length=12)
	private String droitAf;

	@Column(name="DROIT_SU", nullable=false, length=12)
	private String droitSu;

	@Column(name="PRENOM_ENF", nullable=false, length=32)
	private String prenomEnf;

	@Column(name="RANG_ENF", nullable=false, length=11)
	private String rangEnf;

	@ManyToOne
	@JoinColumn(name="MATRICULE", nullable=false)
	private Client client;
	public Consultdroitalloc() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getCle() {
		return this.cle;
	}

	public void setCle(BigDecimal cle) {
		this.cle = cle;
	}

	public String getCodeSusp() {
		return this.codeSusp;
	}

	public void setCodeSusp(String codeSusp) {
		this.codeSusp = codeSusp;
	}

	public String getDateDepartDroits() {
		return this.dateDepartDroits;
	}

	public void setDateDepartDroits(String dateDepartDroits) {
		this.dateDepartDroits = dateDepartDroits;
	}

	public String getDateFinDroits() {
		return this.dateFinDroits;
	}

	public void setDateFinDroits(String dateFinDroits) {
		this.dateFinDroits = dateFinDroits;
	}

	public String getDateNaissance() {
		return this.dateNaissance;
	}

	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getDroitAf() {
		return this.droitAf;
	}

	public void setDroitAf(String droitAf) {
		this.droitAf = droitAf;
	}

	public String getDroitSu() {
		return this.droitSu;
	}

	public void setDroitSu(String droitSu) {
		this.droitSu = droitSu;
	}

	public String getPrenomEnf() {
		return this.prenomEnf;
	}

	public void setPrenomEnf(String prenomEnf) {
		this.prenomEnf = prenomEnf;
	}

	public String getRangEnf() {
		return this.rangEnf;
	}

	public void setRangEnf(String rangEnf) {
		this.rangEnf = rangEnf;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

}