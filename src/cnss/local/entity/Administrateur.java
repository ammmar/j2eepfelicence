package cnss.local.entity;

import java.io.Serializable;
import javax.persistence.*;


import java.util.List;


/**
 * The persistent class for the ADMINISTRATEUR database table.
 * 
 */
@Entity
@Table(name="ADMINISTRATEUR")
@NamedQuery(name="Administrateur.findAll", query="SELECT a FROM Administrateur a")
public class Administrateur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ADMINISTRATEUR_IDADMIN_GENERATOR", sequenceName="ADMIN_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ADMINISTRATEUR_IDADMIN_GENERATOR")
	@Column(unique=true, nullable=false, precision=10)
	private long idadmin;

	@Column(length=64)
	private String email;

	@Column(nullable=false, length=16)
	private String login;

	@Column(nullable=false, length=64)
	private String nomprenom;

	@Column(nullable=false, length=64)
	private String password;
	
	@OneToMany(mappedBy="administrateur")
	private List<Reponse> reponses;

	public Administrateur() {
	}

	public long getIdadmin() {
		return this.idadmin;
	}

	public void setIdadmin(long idadmin) {
		this.idadmin = idadmin;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNomprenom() {
		return this.nomprenom;
	}

	public void setNomprenom(String nomprenom) {
		this.nomprenom = nomprenom;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Reponse> getReponses() {
		return this.reponses;
	}

	public void setReponses(List<Reponse> reponses) {
		this.reponses = reponses;
	}

	public Reponse addRepons(Reponse repons) {
		getReponses().add(repons);
		repons.setAdministrateur(this);

		return repons;
	}

	public Reponse removeRepons(Reponse repons) {
		getReponses().remove(repons);
		repons.setAdministrateur(null);

		return repons;
	}

	@Override
	public String toString() {
		return "Administrateur [email=" + email + ", login=" + login + ", nomprenom=" + nomprenom + ", password="
				+ password + "]";
	}

	
	
}