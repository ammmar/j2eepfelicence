package cnss.local.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the TRAITEMENT database table.
 * 
 */
@Entity
@Table(name="TRAITEMENT")
@NamedQuery(name="Traitement.findAll", query="SELECT t FROM Traitement t")
public class Traitement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="TRT_CODE", unique=true, nullable=false, precision=22)
	private long trtCode;

	@Column(name="TRT_DELAIS", precision=22)
	private BigDecimal trtDelais;

	@Column(name="TRT_LIB", length=100)
	private String trtLib;

	@Column(name="TRT_LIB_AR", length=100)
	private String trtLibAr;

	@OneToMany(mappedBy="traitement")
	private List<HistSuiviDoss> histSuiviDosses;

	public Traitement() {
	}

	public long getTrtCode() {
		return this.trtCode;
	}

	public void setTrtCode(long trtCode) {
		this.trtCode = trtCode;
	}

	public BigDecimal getTrtDelais() {
		return this.trtDelais;
	}

	public void setTrtDelais(BigDecimal trtDelais) {
		this.trtDelais = trtDelais;
	}

	public String getTrtLib() {
		return this.trtLib;
	}

	public void setTrtLib(String trtLib) {
		this.trtLib = trtLib;
	}

	public String getTrtLibAr() {
		return this.trtLibAr;
	}

	public void setTrtLibAr(String trtLibAr) {
		this.trtLibAr = trtLibAr;
	}

	public List<HistSuiviDoss> getHistSuiviDosses() {
		return this.histSuiviDosses;
	}

	public void setHistSuiviDosses(List<HistSuiviDoss> histSuiviDosses) {
		this.histSuiviDosses = histSuiviDosses;
	}

	public HistSuiviDoss addHistSuiviDoss(HistSuiviDoss histSuiviDoss) {
		getHistSuiviDosses().add(histSuiviDoss);
		histSuiviDoss.setTraitement(this);

		return histSuiviDoss;
	}

	public HistSuiviDoss removeHistSuiviDoss(HistSuiviDoss histSuiviDoss) {
		getHistSuiviDosses().remove(histSuiviDoss);
		histSuiviDoss.setTraitement(null);

		return histSuiviDoss;
	}

	@Override
	public String toString() {
		return "Traitement [trtCode=" + trtCode + ", trtDelais=" + trtDelais + ", trtLib=" + trtLib + ", trtLibAr="
				+ trtLibAr + ", histSuiviDosses=" + histSuiviDosses + "]";
	}

	
}