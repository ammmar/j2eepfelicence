package cnss.local.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * The persistent class for the CLIENT database table.
 * 
 */
@Entity
@Table(name="CLIENT")
@NamedQuery(name="Client.findAll", query="SELECT c FROM Client c")
public class Client implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CLIENT_MATRICULE_GENERATOR", sequenceName="CLIENT_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CLIENT_MATRICULE_GENERATOR")
	@Column(unique=true, nullable=false, precision=22)
	private long matricule;
	
	
	@Transient
	private String matriculeChaine;

	@Column(nullable=false, precision=22)
	private BigDecimal cle;
	
	@Transient
	private String cleChaine;

	@Column(nullable=false, length=64)
	private String email;

	@Column(nullable=false, precision=10)
	private BigDecimal estactive;

	@Column(nullable=true, precision=10)
	private BigDecimal idclient;

	@Column(nullable=false, length=64)
	private String nomprenom;

	@Column(nullable=false, precision=22)
	private BigDecimal numcin;
	
	@Transient
	private String Chainenumcin;

	@Column(length=16)
	private String numtel;

	@Column(nullable=false, length=64)
	private String password;

	
	//bi-directional many-to-one association to CarriereAssure
	@OneToMany(mappedBy="client")
	private List<CarriereAssure> carriereAssures;

	//bi-directional many-to-one association to Consultdroitalloc
	@OneToMany(mappedBy="client")
	private List<Consultdroitalloc> consultdroitallocs;

	//bi-directional many-to-one association to HistSuiviDoss
	@OneToMany(mappedBy="client")
	private List<HistSuiviDoss> histSuiviDosses;

	//bi-directional many-to-one association to Mandatfamille
	@OneToMany(mappedBy="client")
	private List<Mandatfamille> mandatfamilles;

	//bi-directional many-to-one association to Reclamation
	@OneToMany(mappedBy="client")
	private List<Reclamation> reclamations;
	
	
	@Transient
	private String etatCompte;
	
	public Client() {
		
	}
	public String getMatriculeChaine() {
		return matriculeChaine;
	}
	public void setMatriculeChaine(String matriculeChaine) {
		this.matriculeChaine = matriculeChaine;
		this.matricule=Long.valueOf(this.matriculeChaine);
	}
	
	public String getCleChaine()
	{
		return cleChaine;
	}
	public void setCleChaine(String cleChaine) {
		this.cleChaine = cleChaine;
		this.cle=BigDecimal.valueOf(Long.valueOf(this.cleChaine));
	}

	public String getChainenumcin()
	{
		return Chainenumcin;
	}

	public void setChainenumcin(String chainenumcin) 
	{
		Chainenumcin = chainenumcin;
		BigDecimal numc=BigDecimal.valueOf(Long.valueOf(chainenumcin));
		this.numcin=numc;
	}


	public String getEtatCompte()
	{
		return etatCompte;
	}

	public void setEtatCompte(String etatCompte)
	{
		this.etatCompte = etatCompte;
	}
	public Client(long matricule, BigDecimal cle, String email, BigDecimal estactive, BigDecimal idclient,
			String nomprenom, BigDecimal numcin, String numtel, String password, List<CarriereAssure> carriereAssures,
			List<Consultdroitalloc> consultdroitallocs, List<HistSuiviDoss> histSuiviDosses,
			List<Mandatfamille> mandatfamilles, List<Reclamation> reclamations, String etatCompte) {
		super();
		this.matricule = matricule;
		this.cle = cle;
		this.email = email;
		this.estactive = estactive;
		this.idclient = idclient;
		this.nomprenom = nomprenom;
		this.numcin = numcin;
		this.numtel = numtel;
		this.password = password;
		this.carriereAssures = carriereAssures;
		this.consultdroitallocs = consultdroitallocs;
		this.histSuiviDosses = histSuiviDosses;
		this.mandatfamilles = mandatfamilles;
		this.reclamations = reclamations;
		this.etatCompte = etatCompte;
	}

	public long getMatricule() {
		return this.matricule;
	}

	public void setMatricule(long matricule) {
		this.matricule = matricule;
	}

	public BigDecimal getCle() {
		return this.cle;
	}

	public void setCle(BigDecimal cle) {
		this.cle = cle;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BigDecimal getEstactive() {
		return this.estactive;
	}

	public void setEstactive(BigDecimal estactive) {
		this.estactive = estactive;
	}

	public BigDecimal getIdclient() {
		return this.idclient;
	}

	public void setIdclient(BigDecimal idclient) {
		this.idclient = idclient;
	}

	public String getNomprenom() {
		return this.nomprenom;
	}

	public void setNomprenom(String nomprenom) {
		this.nomprenom = nomprenom;
	}

	public BigDecimal getNumcin() {
		return this.numcin;
	}

	public void setNumcin(BigDecimal numcin) {
		this.numcin = numcin;
	}

	public String getNumtel() {
		return this.numtel;
	}

	public void setNumtel(String numtel) {
		this.numtel = numtel;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<CarriereAssure> getCarriereAssures() {
		return this.carriereAssures;
	}

	public void setCarriereAssures(List<CarriereAssure> carriereAssures) {
		this.carriereAssures = carriereAssures;
	}

	public CarriereAssure addCarriereAssure(CarriereAssure carriereAssure) {
		getCarriereAssures().add(carriereAssure);
		carriereAssure.setClient(this);

		return carriereAssure;
	}

	public CarriereAssure removeCarriereAssure(CarriereAssure carriereAssure) {
		getCarriereAssures().remove(carriereAssure);
		carriereAssure.setClient(null);

		return carriereAssure;
	}

	public List<Consultdroitalloc> getConsultdroitallocs() {
		return this.consultdroitallocs;
	}

	public void setConsultdroitallocs(List<Consultdroitalloc> consultdroitallocs) {
		this.consultdroitallocs = consultdroitallocs;
	}

	public Consultdroitalloc addConsultdroitalloc(Consultdroitalloc consultdroitalloc) {
		getConsultdroitallocs().add(consultdroitalloc);
		consultdroitalloc.setClient(this);

		return consultdroitalloc;
	}

	public Consultdroitalloc removeConsultdroitalloc(Consultdroitalloc consultdroitalloc) {
		getConsultdroitallocs().remove(consultdroitalloc);
		consultdroitalloc.setClient(null);

		return consultdroitalloc;
	}

	public List<HistSuiviDoss> getHistSuiviDosses() {
		return this.histSuiviDosses;
	}

	public void setHistSuiviDosses(List<HistSuiviDoss> histSuiviDosses) {
		this.histSuiviDosses = histSuiviDosses;
	}

	public HistSuiviDoss addHistSuiviDoss(HistSuiviDoss histSuiviDoss) {
		getHistSuiviDosses().add(histSuiviDoss);
		histSuiviDoss.setClient(this);
		return histSuiviDoss;
	}

	public HistSuiviDoss removeHistSuiviDoss(HistSuiviDoss histSuiviDoss) {
		getHistSuiviDosses().remove(histSuiviDoss);
		histSuiviDoss.setClient(null);
		return histSuiviDoss;
	}

	public List<Mandatfamille> getMandatfamilles() {
		return this.mandatfamilles;
	}

	public void setMandatfamilles(List<Mandatfamille> mandatfamilles) {
		this.mandatfamilles = mandatfamilles;
	}
	
	public Mandatfamille addMandatfamille(Mandatfamille mandatfamille) {
		getMandatfamilles().add(mandatfamille);
		mandatfamille.setClient(this);
		return mandatfamille;
	}
	
	public Mandatfamille removeMandatfamille(Mandatfamille mandatfamille) {
		getMandatfamilles().remove(mandatfamille);
		mandatfamille.setClient(null);
		return mandatfamille;
	}
	
	public List<Reclamation> getReclamations() {
		return this.reclamations;
	}
	public void setReclamations(List<Reclamation> reclamations) {
		this.reclamations = reclamations;
	}
	public Reclamation addReclamation(Reclamation reclamation) {
		getReclamations().add(reclamation);
		reclamation.setClient(this);
		return reclamation;
	}

	public Reclamation removeReclamation(Reclamation reclamation) {
		getReclamations().remove(reclamation);
		reclamation.setClient(null);
		return reclamation;
	}
	@Override
	public String toString() {
		return "Client [matricule=" + matricule + ", email=" + email + ", estactive=" + estactive + ", idclient="
				+ idclient + ", nomprenom=" + nomprenom + ", numcin=" + numcin + ", numtel=" + numtel + ", password="
				+ password + "]"+etatCompte;
	}
	
	

}