package cnss.local.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the TYPERECLAMATIONS database table.
 * 
 */
@Entity
@Table(name="TYPERECLAMATIONS")
@NamedQuery(name="Typereclamation.findAll", query="SELECT t FROM Typereclamation t")
public class Typereclamation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, precision=10)
	private long idtype;

	@Column(nullable=false, length=32)
	private String description;

	@OneToMany(mappedBy="typereclamation")
	private List<Reclamation> reclamations;

	public Typereclamation() {

	}
	
	public Typereclamation(String desc) {
		this.description=desc;
	}
	
	
	public Typereclamation(long idtype, String description) {
		super();
		this.idtype = idtype;
		this.description = description;
	}
	public long getIdtype() {
		return this.idtype;
	}

	public void setIdtype(long idtype) {
		this.idtype = idtype;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Reclamation> getReclamations() {
		return this.reclamations;
	}

	public void setReclamations(List<Reclamation> reclamations) {
		this.reclamations = reclamations;
	}

	public Reclamation addReclamation(Reclamation reclamation) {
		getReclamations().add(reclamation);
		reclamation.setTypereclamation(this);

		return reclamation;
	}

	public Reclamation removeReclamation(Reclamation reclamation) {
		getReclamations().remove(reclamation);
		reclamation.setTypereclamation(null);

		return reclamation;
	}

}