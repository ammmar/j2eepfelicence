package cnss.local.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CARRIERE_ASSURE database table.
 * 
 */
@Entity
@Table(name="CARRIERE_ASSURE")
@NamedQuery(name="CarriereAssure.findAll", query="SELECT c FROM CarriereAssure c")
public class CarriereAssure implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, precision=10)
	private long id;

	@Column(nullable=false, precision=22)
	private BigDecimal annee;

	@Column(name="ASS_CLE", nullable=false, precision=22)
	private BigDecimal assCle;

	@Column(name="ASS_RACINE", precision=7)
	private BigDecimal assRacine;

	@Column(name="CAR_CAT_INDEP", precision=22)
	private BigDecimal carCatIndep;

	@Column(name="CCP_CODPLCH", precision=22)
	private BigDecimal ccpCodplch;

	@Column(name="CEX_COD", nullable=false, precision=22)
	private BigDecimal cexCod;

	@Column(name="CODE_TYPE_SALAIRE", nullable=false, precision=22)
	private BigDecimal codeTypeSalaire;

	@Column(name="CODE_VALIDATION", precision=22)
	private BigDecimal codeValidation;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_SAISIE")
	private Date dateSaisie;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_VALIDATION")
	private Date dateValidation;

	@Column(name="EMP_CLE", precision=22)
	private BigDecimal empCle;

	@Column(name="EMP_MAT", precision=22)
	private BigDecimal empMat;

	@Column(name="IND_ASS_CLE", precision=22)
	private BigDecimal indAssCle;

	@Column(name="IND_ASS_MAT", precision=22)
	private BigDecimal indAssMat;

	@Column(name="MAT_OPE_RVAL", precision=22)
	private BigDecimal matOpeRval;

	@Column(name="MAT_OPER_SAISI", precision=22)
	private BigDecimal matOperSaisi;

	@Column(name="NUM_SEQ_CARR", nullable=false, precision=22)
	private BigDecimal numSeqCarr;

	@Column(name="REG_COD", nullable=false, precision=22)
	private BigDecimal regCod;

	@Column(precision=22)
	private BigDecimal salaire;

	@Column(name="SCP_COD", precision=22)
	private BigDecimal scpCod;

	@Column(name="SCP_TYPCAT", precision=22)
	private BigDecimal scpTypcat;

	@Column(nullable=false, precision=22)
	private BigDecimal trimestre;

	//bi-directional many-to-one association to Client
	@ManyToOne
	@JoinColumn(name="ASS_MAT", nullable=false)
	private Client client;

	public CarriereAssure() {
		
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getAnnee() {
		return this.annee;
	}

	public void setAnnee(BigDecimal annee) {
		this.annee = annee;
	}

	public BigDecimal getAssCle() {
		return this.assCle;
	}

	public void setAssCle(BigDecimal assCle) {
		this.assCle = assCle;
	}

	public BigDecimal getAssRacine() {
		return this.assRacine;
	}

	public void setAssRacine(BigDecimal assRacine) {
		this.assRacine = assRacine;
	}

	public BigDecimal getCarCatIndep() {
		return this.carCatIndep;
	}

	public void setCarCatIndep(BigDecimal carCatIndep) {
		this.carCatIndep = carCatIndep;
	}

	public BigDecimal getCcpCodplch() {
		return this.ccpCodplch;
	}

	public void setCcpCodplch(BigDecimal ccpCodplch) {
		this.ccpCodplch = ccpCodplch;
	}

	public BigDecimal getCexCod() {
		return this.cexCod;
	}

	public void setCexCod(BigDecimal cexCod) {
		this.cexCod = cexCod;
	}

	public BigDecimal getCodeTypeSalaire() {
		return this.codeTypeSalaire;
	}

	public void setCodeTypeSalaire(BigDecimal codeTypeSalaire) {
		this.codeTypeSalaire = codeTypeSalaire;
	}

	public BigDecimal getCodeValidation() {
		return this.codeValidation;
	}

	public void setCodeValidation(BigDecimal codeValidation) {
		this.codeValidation = codeValidation;
	}

	public Date getDateSaisie() {
		return this.dateSaisie;
	}

	public void setDateSaisie(Date dateSaisie) {
		this.dateSaisie = dateSaisie;
	}

	public Date getDateValidation() {
		return this.dateValidation;
	}

	public void setDateValidation(Date dateValidation) {
		this.dateValidation = dateValidation;
	}

	public BigDecimal getEmpCle() {
		return this.empCle;
	}

	public void setEmpCle(BigDecimal empCle) {
		this.empCle = empCle;
	}

	public BigDecimal getEmpMat() {
		return this.empMat;
	}

	public void setEmpMat(BigDecimal empMat) {
		this.empMat = empMat;
	}

	public BigDecimal getIndAssCle() {
		return this.indAssCle;
	}

	public void setIndAssCle(BigDecimal indAssCle) {
		this.indAssCle = indAssCle;
	}

	public BigDecimal getIndAssMat() {
		return this.indAssMat;
	}

	public void setIndAssMat(BigDecimal indAssMat) {
		this.indAssMat = indAssMat;
	}

	public BigDecimal getMatOpeRval() {
		return this.matOpeRval;
	}

	public void setMatOpeRval(BigDecimal matOpeRval) {
		this.matOpeRval = matOpeRval;
	}

	public BigDecimal getMatOperSaisi() {
		return this.matOperSaisi;
	}

	public void setMatOperSaisi(BigDecimal matOperSaisi) {
		this.matOperSaisi = matOperSaisi;
	}

	public BigDecimal getNumSeqCarr() {
		return this.numSeqCarr;
	}

	public void setNumSeqCarr(BigDecimal numSeqCarr) {
		this.numSeqCarr = numSeqCarr;
	}

	public BigDecimal getRegCod() {
		return this.regCod;
	}

	public void setRegCod(BigDecimal regCod) {
		this.regCod = regCod;
	}

	public BigDecimal getSalaire() {
		return this.salaire;
	}

	public void setSalaire(BigDecimal salaire) {
		this.salaire = salaire;
	}

	public BigDecimal getScpCod() {
		return this.scpCod;
	}

	public void setScpCod(BigDecimal scpCod) {
		this.scpCod = scpCod;
	}

	public BigDecimal getScpTypcat() {
		return this.scpTypcat;
	}

	public void setScpTypcat(BigDecimal scpTypcat) {
		this.scpTypcat = scpTypcat;
	}

	public BigDecimal getTrimestre() {
		return this.trimestre;
	}

	public void setTrimestre(BigDecimal trimestre) {
		this.trimestre = trimestre;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
}