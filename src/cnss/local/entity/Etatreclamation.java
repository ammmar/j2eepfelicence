package cnss.local.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the ETATRECLAMATION database table.
 * 
 */
@Entity
@Table(name="ETATRECLAMATION")
@NamedQuery(name="Etatreclamation.findAll", query="SELECT e FROM Etatreclamation e")
public class Etatreclamation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ETATRECLAMATION_IDER_GENERATOR", sequenceName="ETATRECLAMATION_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ETATRECLAMATION_IDER_GENERATOR")
	@Column(name="ID_ER", unique=true, nullable=false, precision=10)
	private long idEr;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date dateer;

	@Column(name="ETAT_REC", nullable=false, precision=10)
	private BigDecimal etatRec;

	@ManyToOne
	@JoinColumn(name="IDRECLAMATION", nullable=false)
	private Reclamation reclamation;
	@Transient
	private String etatReclamation;

	
	public Etatreclamation() {
	}

	
	
	public String getEtatReclamation() {
		return etatReclamation;
	}

	public void setEtatReclamation(String etatReclamation) {
		this.etatReclamation = etatReclamation;
	}

	public long getIdEr() {
		return this.idEr;
	}

	public void setIdEr(long idEr) {
		this.idEr = idEr;
	}

	public Date getDateer() {
		return this.dateer;
	}

	public void setDateer(Date dateer) {
		this.dateer = dateer;
	}

	public BigDecimal getEtatRec() {
		return this.etatRec;
	}

	public void setEtatRec(BigDecimal etatRec) {
		this.etatRec = etatRec;
	}

	public Reclamation getReclamation() {
		return this.reclamation;
	}

	public void setReclamation(Reclamation reclamation) {
		this.reclamation = reclamation;
	}

	@Override
	public String toString() {
		return "Etatreclamation [idEr=" + idEr + ", dateer=" + dateer + ", etatRec=" + etatRec + ", reclamation="
				+ reclamation + ", etatReclamation=" + etatReclamation + "]";
	}
	

}