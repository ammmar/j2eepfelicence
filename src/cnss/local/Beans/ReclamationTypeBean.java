package cnss.local.Beans;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import cnss.local.entity.Typereclamation;
import cnss.local.service.TypeReclamationService;

/**
 *il faut prendre en consideration que dans tous  les méthodes de la classe managed Bean  il n 'ya pas de validation car il sont déja fait dans les pages XHTML     
 */


@ManagedBean(name="reclamationTypeBean")
@ViewScoped
public class ReclamationTypeBean {

	private String typeReclamation;

	@ManagedProperty("#{typeReclamationService}")
	 TypeReclamationService typeReclamationService ;
	
	public ReclamationTypeBean() 
	{
		
	}

	@PostConstruct
	public void postConstruct(){
		
		
	}
	
	//la role de cette methode et d'ajout une type réclamation 
	public void AjouterTypeReclamation()
	{
		String EtatType="";
		 FacesMessage message = null;
		Typereclamation tr=new Typereclamation(this.typeReclamation);
		EtatType=typeReclamationService.AjouterTypeReclamation(tr);
		if(EtatType.equalsIgnoreCase("NonExiste"))
		{					
           message = new FacesMessage("le type "+this.typeReclamation+"a éte Bien Ajouter");
           FacesContext.getCurrentInstance().addMessage(null, message);
		}
		else if (EtatType.equalsIgnoreCase("Existe"))
		{
			 message = new FacesMessage("cette type est deja existe dans notre system ");
			 FacesContext.getCurrentInstance().addMessage(null, message);
		}
		else  
		{
			 message = new FacesMessage("Probléme de ajout ");
			 FacesContext.getCurrentInstance().addMessage(null, message);
		}	 
	}
	public void onload() throws IOException 
	{
		HttpSession session = (HttpSession) Util.getSession();
		if (session == null) {
			System.out.println("no session created ");
			FacesContext.getCurrentInstance().getExternalContext().redirect("AccessDenied.xhtml?faces-redirect=true");
		} 
	}

	//Les Setter et les Getter 
	public String getTypeReclamation() {
		return typeReclamation;
	}

	public void setTypeReclamation(String typeReclamation) {
		this.typeReclamation = typeReclamation;
	}

	public TypeReclamationService getTypeReclamationService() {
		return typeReclamationService;
	}

	public void setTypeReclamationService(TypeReclamationService typeReclamationService) {
		this.typeReclamationService = typeReclamationService;
	}

	
	
	

	
	
}
