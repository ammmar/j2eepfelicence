package cnss.local.Beans;

import java.io.IOException;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import cnss.local.entity.Administrateur;
import cnss.local.entity.Client;
import cnss.local.service.AdminService;
import cnss.local.service.ClientService;

/**
 *il faut prendre en consiédiration que dans les methodes il n 'ya pas de validation car il sont déja fait dans les pages XHTML     
 */
@ManagedBean(name="authentificationBean")
@ViewScoped
public class AuthentificationBean {
	
	
	@ManagedProperty("#{adminService}")
	private AdminService adminService;
	
	@ManagedProperty("#{clientService}")
	private ClientService clientService;
	
	private String nouveauMotPasse1;
	private String nouveauMotPasse2;
	
	Client c=new Client();
	private Administrateur admin=new Administrateur();

	
	public AuthentificationBean() {
		super();
	}


	@PostConstruct
	public void init() {

	}

//la role de cette methode et  la connexion au interface d'adminstaration aprés  verifier la validite de login et motpasse    
   public void dologin() throws IOException {
        FacesMessage message = null;   
        //cette condition et la vérification de l'existance de login et password avec les methode de la classe adminservice 
        if(this.admin.getLogin().toString()!=""  && this.admin.getPassword().toString() !="" && adminService.Rechercheby2Properties("login",this.admin.getLogin().toString(), "password",this.admin.getPassword().toString()).size()>0 ) {

        		FacesContext facesContext = FacesContext.getCurrentInstance();
        	   ExternalContext externalContext = facesContext.getExternalContext();
        	    HttpSession session = (HttpSession)Util.getSession();       	  
        	   session.setAttribute("admin",this.adminService.Rechercheby2Properties("login",this.admin.getLogin().toString(),"password",this.admin.getPassword().toString()).get(0));
        	   externalContext.redirect("Accueil.xhtml");
        	    
        } 
        //message d'érreur
        else {          
            message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "verifier login ou mot passe svp","svp");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
         
    
    }
   
 //la role de cette methode el la déconexion 
   public void doLogout() throws IOException
   {
	   FacesContext facesContext = FacesContext.getCurrentInstance();
	   HttpSession session = (HttpSession)Util.getSession();
	   ExternalContext externalContext = facesContext.getExternalContext();
	   session.invalidate();
	   externalContext.redirect("Authentification.xhtml");   
   }
   
   
   public void logout() throws IOException
   {
	   FacesContext facesContext = FacesContext.getCurrentInstance();
	   HttpSession session = (HttpSession)Util.getSession();
	   ExternalContext externalContext = facesContext.getExternalContext();
	   session.invalidate();
	   externalContext.redirect("Authentification.xhtml");   
   }
   

   
   //Setter +Getter de la classe AuthentificationBean
	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}

	public Client getC() {
		return c;
	}

	public void setC(Client c) {
		this.c = c;
	}

	public AdminService getAdminService() {
		return adminService;
	}

	public void setAdminService(AdminService adminService) {
		this.adminService = adminService;
	}
   

	public Administrateur getAdmin() {
		return admin;
	}

	public void setAdmin(Administrateur admin) {
		this.admin = admin;
	}

	public String getNouveauMotPasse1() {
		return nouveauMotPasse1;
	}

	public void setNouveauMotPasse1(String nouveauMotPasse1) {
		this.nouveauMotPasse1 = nouveauMotPasse1;
	}

	public String getNouveauMotPasse2() {
		return nouveauMotPasse2;
	}

	public void setNouveauMotPasse2(String nouveauMotPasse2) {
		this.nouveauMotPasse2 = nouveauMotPasse2;
	}

}
