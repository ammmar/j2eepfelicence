package cnss.local.Beans;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import cnss.local.entity.Administrateur;
import cnss.local.service.AdminService;
@ManagedBean(name = "accueilBean")
@ViewScoped
public class AccueilBean {
	@ManagedProperty("#{adminService}")
	private AdminService adminService;
	public AccueilBean() {

	}
	@PostConstruct
	public void init() {

	}

	public void onload() throws IOException {
		HttpSession session = (HttpSession) Util.getSession();
		try {
			if (session == null) {
				System.out.println("no session created ");
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("AccessDenied.xhtml?faces-redirect=true");
			} else {
				Administrateur a = new Administrateur();
				a = (Administrateur) session.getAttribute("admin");
				if (a == null) {
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("AccessDenied.xhtml?faces-redirect=true");
				}
			}
		} catch (Exception e) {
			System.out.println("excpetion" + e.getMessage());

		}
		
	}

	
	public String getCurrentNameOfuser() throws IOException {

		String user = "ErrNoUserFound";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		ExternalContext externalContext = facesContext.getExternalContext();
		if (session.getAttribute("admin").toString().length() == 0) {
			System.out.println("not connected yed ");
			externalContext.redirect("Autentification.xhtml");
		} else {
			Administrateur a = (Administrateur) session.getAttribute("admin");
			if (user != null) {
				user = a.getNomprenom();
			}
		
		}
		return user;
	}
	
	public AdminService getAdminService() {
		return adminService;
	}

	public void setAdminService(AdminService adminService) {
		this.adminService = adminService;
	}
	
}
