package cnss.local.Beans;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import cnss.local.entity.Administrateur;
import cnss.local.entity.Etatreclamation;
import cnss.local.entity.Reclamation;
import cnss.local.entity.Reponse;
import cnss.local.service.ReclamationService;
import cnss.local.service.ReponseService;
@ManagedBean(name ="reclamationBean")
@ViewScoped

/**
 *il faut prendre en consideration que dans tous  les méthodes de la classe managed Bean  il n 'ya pas de validation car il sont déja fait dans les pages XHTML     
 */


public class ReclamationBean {
	@ManagedProperty("#{reclamationService}")
	ReclamationService reclamationService;	
	@ManagedProperty("#{reponseService}")
	ReponseService reponseService;
	List<Reclamation> listReclamation = new ArrayList<Reclamation>();
	Reponse newReponse = new Reponse();
	Reclamation r = new Reclamation();
	private String Che=new String();
	private ArrayList<Reponse>ListReponseAdminRec=new ArrayList<Reponse>();
	private List<Etatreclamation> ListEtat;
	 long idReclamation;
	
		public ReclamationBean()
		{
			
	    }
		//dans cette méthode le Managed bean est entièrement initialisé
		@PostConstruct
		public void postConstruct() {
			//chargement des Réclamation a partir de la base de données 
			this.listReclamation = (ArrayList<Reclamation>) reclamationService.getAllreclamation();
			for (int i = 0; i < this.listReclamation.size(); i++)
			{
				this.listReclamation.get(i).setLastDate(
						String.valueOf(this.GetLastDateEtatReclamation(this.listReclamation.get(i)).getDateer()));
				if (String.valueOf(this.GetLastDateEtatReclamation(this.listReclamation.get(i)).getEtatRec()).equals("0")) 
				{
					this.listReclamation.get(i).setLastDateEtatValue("non traite");
				}
				else
				{
					this.listReclamation.get(i).setLastDateEtatValue("traite");
				}

			}
		}
	 //cette methode il retourne la derniére date de l'état d'une Réclamation donnée. <<Etat(traite/non traité)>>
	public Etatreclamation GetLastDateEtatReclamation(Reclamation r) {
		List<Etatreclamation> ListE;
		ListE = r.getEtatreclamations();
		Etatreclamation er = new Etatreclamation();
		er = ListE.get(0);
		Date df = ListE.get(0).getDateer();
		for (int i = 0; i < ListE.size(); i++) {
			if (ListE.get(i).getDateer().compareTo(df) > 0) 
			{
				df = ListE.get(i).getDateer();
				er = ListE.get(i);
			}
		}
		return er;
	}
	
	 //la role de cette méthode el la mise a jour de l'attribut r pour l'utiliuser dans l'inteface répondre au Réclamation  
	public void putReclamationReponse(Reclamation rec) 
	{
		this.r=rec;
	}
	
    //cette méthode il fait la Réponse du Réclamation
	public void saveReponse() 
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		Administrateur a =(Administrateur) session.getAttribute("admin");
		Etatreclamation etat = this.GetLastDateEtatReclamation(this.r);
		this.newReponse.setDatereponse(new Date());
		this.newReponse.setAdministrateur(a);
		this.newReponse.setReclamation(this.r);
		etat.setEtatRec(new BigDecimal(Che));
		etat.setDateer(new Date());
		//mise a jour de l' EtatReclamation par la dérniére état de la  Reclamation répondue par l'administarteur 
		this.reclamationService.saveEtat(etat);
		
		//mise a jour de la  reponse de ceette  réclamation 
		this.reclamationService.SaveReponse(this.newReponse);
		//message d'information dans la IHM Adminstrateur
		FacesMessage message = new FacesMessage("tu est Répond au Réclamation  n  "+this.r.getIdreclamation());
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	   // méthode de déconexion qui fait la rédirection au page  Authentification.xhtml
	 public void doLogout() throws IOException
	   {
		   FacesContext facesContext = FacesContext.getCurrentInstance();
		   
		   HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		   
		   ExternalContext externalContext = facesContext.getExternalContext();
		   externalContext.invalidateSession();
		   externalContext.redirect("Autentification.xhtml");
	   }


	//la role de cette methode et la vérification  de la bon  utilsateur qui accede a chacune des pages de l'application  
	public void onload() throws IOException 
	{
		HttpSession session = (HttpSession) Util.getSession();
		if (session == null) {
			System.out.println("no session created ");
			FacesContext.getCurrentInstance().getExternalContext().redirect("AccessDenied.xhtml?faces-redirect=true");
		} 
		
	}
	//Les setter et Les Getter
	 public Reclamation getR() 
	 {
		return r;
	}

	public void setR(Reclamation r) 
	{
		this.r = r;
	}

	public String getChe() 
	{
		return Che;
	}

	public void setChe(String che) 
	{
		Che = che;
	}

	public List<Etatreclamation> getListEtat()
	{
		return ListEtat;
	}

	public void setListEtat(List<Etatreclamation> listEtat) 
	{
		ListEtat = listEtat;
	}

	

	public ReponseService getReponseService()
	{
		return reponseService;
	}

	public void setReponseService(ReponseService reponseService) 
	{
		this.reponseService = reponseService;
	}

	public Reponse getNewReponse()
	{
		return newReponse;
	}

	public void setNewReponse(Reponse newReponse) 
	{
		this.newReponse = newReponse;
	}

	public ReclamationService getReclamationService() 
	{
		return reclamationService;
	}

	public void setReclamationService(ReclamationService reclamationService)
	{
		this.reclamationService = reclamationService;
	}

	public List<Reclamation> getlistReclamation() 
	{
		return listReclamation;
	}

	public void setlistReclamation(List<Reclamation> listReclamation) 
	{
		this.listReclamation = listReclamation;
	}
	
	public long getIdReclamation() 
	{
		return idReclamation;
	}

	public void setIdReclamation(long idReclamation) 
	{
		this.idReclamation = idReclamation;
	}
	
}
