package cnss.local.Beans;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import cnss.local.entity.Client;
import cnss.local.service.AdminService;
import cnss.local.service.ClientService;

/**
 *il faut prendre en consideration que dans tous  les méthodes de la classe managed Bean  il n 'ya pas de validation car il sont déja fait dans les pages XHTML     
 */
@ManagedBean(name = "clientBean")
@ViewScoped
public class ClientBean 
{
	@ManagedProperty("#{clientService}")
	private ClientService clientService;	
	@ManagedProperty("#{adminService}")
	private AdminService adminService;
	private List<Client> ListClient = new ArrayList<Client>();
	private List<Client> ListClientinfo;
	private List<Client> filteredClient;
	private Client c = new Client();
	private Client cn=new Client();
	private Client cltupdate=new Client();
	private String nouveauMotPasse1;
	private String nouveauMotPasse2;
	private Client cMiseAjourMotPasse=new Client();
	
	//dans cette méthode le Managed bean est entièrement initialisé
	@PostConstruct
	public void postConstruct() {
		//chargement des client a partir de la base de données 
		this.ListClient = (ArrayList<Client>) this.clientService.getAllClient();
		
		//mise a jour de liste dans l'attribut EtatCompte à travers  l'attribut est active  pour que l'administrateur compendre la signification réel de le champs estactive 
		for (int i = 0; i < this.ListClient.size(); i++) {
			if (String.valueOf(this.ListClient.get(i).getEstactive()).equals("1")) {
				this.ListClient.get(i).setEtatCompte("Activee");
			
			} else if (String.valueOf(this.ListClient.get(i).getEstactive()).equals("0")) {
				this.ListClient.get(i).setEtatCompte("Bloquee");
			}
		
		}

	}
	//la role de cette méthode el le mise a jour du client  
	public void updateClient() {
		int a = 1;
		int b = 0;
		 Client Clt=new Client();
		 Clt=this.clientService.Rechercheby1Properties("matricule", this.cltupdate.getMatricule()).get(0);
		 Clt.setNomprenom(this.cltupdate.getNomprenom());
		 Clt.setEmail(this.cltupdate.getEmail());
		 Clt.setNumtel(this.cltupdate.getNumtel());
			this.clientService.updateClient(Clt);
			FacesMessage message = null;
			message = new FacesMessage("mise A jour de client de client avec réussite ");
			FacesContext.getCurrentInstance().addMessage(null, message);
			System.out.println(" apres clique  client verif here   !!" + Clt.toString());
			this.cltupdate=new Client();
	}
	
	//la role de cette méthode est  d'ajout des comptes au système   
	public void saveClient() {
		int a = 1;
		int b = 0;
		boolean bool = true;
			
		if (this.cn.getEtatCompte().equalsIgnoreCase("Activee")) {
			this.cn.setEstactive(BigDecimal.valueOf(Long.valueOf(String.valueOf(a))));
			System.out.println("est activee" + this.cn.getEstactive());
		} else if (cn.getEtatCompte().equalsIgnoreCase("Bloquee")) {
			this.cn.setEstactive(BigDecimal.valueOf(Long.valueOf(String.valueOf(b))));
			System.out.println("est activee" + this.cn.getEstactive());

		}

		else if (this.clientService.Rechercheby1Properties("matricule", this.cn.getMatricule()).size()>0 )
		{
			FacesMessage message = null;
			message = new FacesMessage("cette matricule est dèja utilisée par un autre client ");
			FacesContext.getCurrentInstance().addMessage(null, message);
			System.out.println(" aprés clique  client verif" + cn.toString());
			bool = false;
		}
		if(bool)
		{
			this.clientService.saveClient(this.cn);
			FacesMessage message = null;
			message = new FacesMessage("mise A jour de client de client avec réussite ");
			FacesContext.getCurrentInstance().addMessage(null, message);
			System.out.println(" apres clique  client verif" + cn.toString());
		}
	}
	
	
	
	
	
	
	//la role de cette methode il fait la modification de l'état du client 
	public void updateEtatClient() {
		int a = 1;
		int b = 0;	
		if (cltupdate.getEtatCompte().equalsIgnoreCase("Activee")) {
			cltupdate.setEstactive(BigDecimal.valueOf(Long.valueOf(String.valueOf(a))));
		
		} else if (cltupdate.getEtatCompte().equalsIgnoreCase("Bloquee")) {
			cltupdate.setEstactive(BigDecimal.valueOf(Long.valueOf(String.valueOf(b))));
		
		}
		  FacesMessage message = null;
			message = new FacesMessage("mise A jour de client de client avec réussite ");
			FacesContext.getCurrentInstance().addMessage(null, message);
			System.out.println(" apres clique  client verif" +this.cltupdate);
	    }
	 
	 
	//la role de cette methode il fait  le Mise à jour de l'ancien motpasse par un nouveau motpasse aprés saisir de la matricule du client 
	   public void MiseAJourMotPasse()
	   {   
		   //le methode Rechercheby2Properties de la classe fait la Recherche de deux Propertie chosit dans les parametre de methode 
		   if(this.clientService.Rechercheby2Properties("password", this.cMiseAjourMotPasse.getPassword(), "matricule", this.cMiseAjourMotPasse.getMatricule()).size()==0 ) 
		   {
			   FacesMessage message=null;
			   message = new FacesMessage("ancien mot passe ou matricule  et incorrecte");
		       FacesContext.getCurrentInstance().addMessage(null, message);
		   }
		   else if(!this.nouveauMotPasse2.equalsIgnoreCase(this.nouveauMotPasse1))
		   {
			   FacesMessage message=null;
			   message = new FacesMessage("vérifier le saisir de  la noveau Mot Passe ");
		       FacesContext.getCurrentInstance().addMessage(null, message);
		   }
		   else{
			   //le methode Rechercheby2Properties de la classe fait la Recherche de Propertie chosit dans les parametre de methode 
			  Client clt= this.clientService.Rechercheby1Properties("matricule", this.cMiseAjourMotPasse.getMatricule()).get(0);
			  clt.setPassword(this.nouveauMotPasse2);
			  this.clientService.saveOrUpdateClient(clt);
			  
			   FacesMessage message=null;
			   message = new FacesMessage("Mise A jour Fait Avec Réussite ");
			   FacesContext.getCurrentInstance().addMessage(null, message);
		   }
		   
		 
	   }
	   
	   
	   //la role de cette méthode el la mise a jour de l'attribut cltupdate pour l'utiliuser dans l'inteface de modifcation du client 
	   public void remplirFormClient(Client client) 
		{
			this.cltupdate=client;
			System.out.println("le valeur de clique de fn remplirFormClient2==>"+this.cltupdate.toString());
		}
		
	   // méthode de déconexion qui fait la rédirection au page  Authentification.xhtml
	   public void doLogout() throws IOException
	   {
		   FacesContext facesContext = FacesContext.getCurrentInstance();   
		  // HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		   ExternalContext externalContext = facesContext.getExternalContext();
		   externalContext.invalidateSession();
		   externalContext.redirect("Authentification.xhtml");
		   
	   }
	   
//la role de cette methode et la vérification  de la bon  utilsateur qui accede a chacune des pages de l'application  
	public void onload() throws IOException {

		HttpSession session = (HttpSession) Util.getSession();
		if (session == null) {
			System.out.println("no session created ");
			FacesContext.getCurrentInstance().getExternalContext().redirect("AccessDenied.xhtml?faces-redirect=true");
		} 
	}
	
	
	/**
	 * 
	 * Setter et les Getter
	 */
	public Client getcMiseAjourMotPasse() {
		return cMiseAjourMotPasse;
	}

	public void setcMiseAjourMotPasse(Client cMiseAjourMotPasse) {
		this.cMiseAjourMotPasse = cMiseAjourMotPasse;
	}

	public String getNouveauMotPasse1() {
		return nouveauMotPasse1;
	}

	public void setNouveauMotPasse1(String nouveauMotPasse1) {
		this.nouveauMotPasse1 = nouveauMotPasse1;
	}

	public String getNouveauMotPasse2() {
		return nouveauMotPasse2;
	}

	public void setNouveauMotPasse2(String nouveauMotPasse2) {
		this.nouveauMotPasse2 = nouveauMotPasse2;
	}

	public AdminService getAdminService() {
		return adminService;
	}

	public void setAdminService(AdminService adminService) {
		this.adminService = adminService;
	}

	public Client getCn() {
		return cn;
	}

	public void setCn(Client cn) {
		this.cn = cn;
	}

	public Client getcltupdate() {
		return cltupdate;
	}

	public void setcltupdate(Client cltupdate) {
		this.cltupdate = cltupdate;
	}

	public List<Client> getFilteredClient() {
		return filteredClient;
	}

	public void setFilteredClient(List<Client> filteredClient) {
		this.filteredClient = filteredClient;
	}

	public Client getC() {
		return c;
	}

	public void setC(Client c) {
		this.c = c;
	}

	public List<Client> getListClientinfo() {
		return ListClientinfo;
	}

	public void setListClientinfo(List<Client> listClientinfo) {
		ListClientinfo = listClientinfo;
	}

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {

		this.clientService = clientService;
	}

	public List<Client> getListClient() {

		return ListClient;
	}

	public void setListClient(List<Client> listClient) {
		ListClient = listClient;
	}

	

}
