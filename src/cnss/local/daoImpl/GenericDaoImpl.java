package cnss.local.daoImpl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cnss.local.dao.GenericDao;
/**
*voila cette est la classe et la classe Resposnable a la communication avec la base de de données
*cette classe une classe generic  C.A.D il est utiliser par toute les autre classe DAO
* el contient des methode génralement utilser dans tous les classe Dao et le  But principal est  la recriture de la méme code
**/

//voila c'est design pattern utlise pour la forte couplage avec le classe spécifié dans le parmétre 
@Repository("genericDao")
public abstract class GenericDaoImpl<E, PK extends java.io.Serializable> implements GenericDao<E, PK>
{
  
     
	
	/**
	 * /session factory ces parmi les choses spécifique de Hibernate cette attribut el est reponsable d'ouvrir la seesion avec la base de donnes
	 */
    @Autowired
    @Qualifier("sessionFactory")
	private SessionFactory sessionFactory;  //comes in from bean
     
    private Class<E> entityClass;
 
    //constructors
    @SuppressWarnings("unchecked")
	public GenericDaoImpl() {
        this.entityClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        //System.out.println(this.entityClass+"eeeeeeeee");
    }
 
    //getters and setters
    public SessionFactory getSessionFactory() {
        if( sessionFactory == null)
            throw new IllegalStateException("SessionFactory has not been set on DAO before usage");
        return sessionFactory;
    }
 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
 
    public Class<E> getEntityClass() {
        return entityClass;
    }
 
    public void setEntityClass(Class<E> entityClass) {
        this.entityClass = entityClass;
    }
     
    
    //tous les methode Responsable a la communciation avec la base de donnes 
    // CRUD stuff
    @SuppressWarnings("unchecked")
    @Transactional(readOnly=false)
	public PK save(E newInstance)
    {
        return (PK) this.sessionFactory.getCurrentSession().save(newInstance);
    }
    @Transactional(readOnly=false)
    public void update(E transientObject)
    {
        this.sessionFactory.getCurrentSession().update(transientObject);
    }
	
    @Transactional(readOnly=false)
    public void saveOrUpdate(E transientObject)
    {
        this.sessionFactory.getCurrentSession().saveOrUpdate(transientObject);        
    }

    public void delete(E persistentObject)
    {
        this.sessionFactory.getCurrentSession().delete(persistentObject);
    }
 
    @SuppressWarnings("unchecked")
	public E findById(PK id)
    {
        List<E> results = null;
        E result = null;
        //Session s = sessionFactory.openSession();
        Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(entityClass);   
        crit.add(Restrictions.eq("id", id));
        results = crit.list();
        if( !results.isEmpty()) {
            if( results.size() == 1) {
                result = results.get(0);
            }
        }
         
        return result;
    }
    
   
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
	public List<E> findAll()
    {
        List<E> results = null;
         
 
        if( this.entityClass == null) {
            System.out.println("entityName is null");
        } else {
            System.out.println("entityName is " +  this.entityClass.getName());
        }
// good so far        
        if( this.sessionFactory == null) {
            System.out.println("sessionFactory is null");   //  OOPS
        } else {
            if( this.sessionFactory.getCurrentSession() == null) {
                System.out.println("session is null");
            } else {
                Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(entityClass);
                if( crit == null) {
                    System.out.println("criteria is null");
                } else {
                    results = crit.list();
                }
            }
        }
                 
        return results;
    }
 
    
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
   	public List<E> findAllBy2Properties(String propertyName1, Object value1,String propertyName2, Object value2)
       {
       	Query query =  sessionFactory.getCurrentSession().createQuery("from "+ getEntityClass().getName() + " u where u."+propertyName1+" = ? and u."+propertyName2+" = ? ");
       	query.setParameter(0,value1);
       	query.setParameter(1,value2);
       	return query.list();

       }
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
 	public List<E> findAllBy1Properties(String propertyName1, Object value1)
     {
     	Query query =  sessionFactory.getCurrentSession().createQuery("from "+ getEntityClass().getName() + " u where u."+propertyName1+" = ?");
     	
     	query.setParameter(0,value1);
     	return query.list();

     }
   
}