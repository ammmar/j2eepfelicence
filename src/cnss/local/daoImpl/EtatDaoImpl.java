package cnss.local.daoImpl;

import org.springframework.stereotype.Repository;

import cnss.local.dao.EtatDao;
import cnss.local.entity.Etatreclamation;


//voila cette classe est la classe et la classe Resposnable a la communication avec la base de de données
@Repository("etatDao")
public class EtatDaoImpl extends GenericDaoImpl<Etatreclamation, Long>  implements EtatDao {

}
