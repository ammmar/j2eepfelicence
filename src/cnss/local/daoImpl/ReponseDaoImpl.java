package cnss.local.daoImpl;



import org.springframework.stereotype.Repository;

import cnss.local.dao.ReponseDao;
import cnss.local.entity.Reponse;

//voila cette classe est la classe et la classe Resposnable a la communication avec la base de de données
@Repository("reponseDao")
public class ReponseDaoImpl  extends GenericDaoImpl<Reponse, Long> implements ReponseDao{

}
