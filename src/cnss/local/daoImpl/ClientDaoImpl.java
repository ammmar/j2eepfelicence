package cnss.local.daoImpl;


import org.springframework.stereotype.Repository;
import cnss.local.dao.ClientDao;
import cnss.local.entity.Client;

//voila cette classe est la classe et la classe Resposnable a la communication avec la base de de données
@Repository("clientDao")
public class ClientDaoImpl extends GenericDaoImpl<Client, Long> implements ClientDao 
{
	
}
