package cnss.local.daoImpl;

import org.springframework.stereotype.Repository;

import cnss.local.dao.AdminDao;
import cnss.local.entity.Administrateur;


//voila cette classe est la classe et la classe Resposnable a la communication avec la base de de données
@Repository("adminDao")
public class AdminDaoImpl extends GenericDaoImpl<Administrateur, Long>  implements AdminDao{
	
}
