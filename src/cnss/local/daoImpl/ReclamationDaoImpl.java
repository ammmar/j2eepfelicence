package cnss.local.daoImpl;

import org.springframework.stereotype.Repository;

import cnss.local.dao.ReclamationDao;
import cnss.local.entity.Reclamation;
//voila cette classe est la classe et la classe Resposnable a la communication avec la base de de données
@Repository("reclamationDao")
public class ReclamationDaoImpl extends GenericDaoImpl<Reclamation,Long> implements ReclamationDao {

}
