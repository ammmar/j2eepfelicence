package cnss.local.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cnss.local.dao.AdminDao;
import cnss.local.entity.Administrateur;
import cnss.local.service.AdminService;
/**
 * 
 *la classe service elle est considérer comme 
 *la logique métier dans l'application c'est ta dire les calcul (Bussiness) 
 *
 */
@Service("adminService")
public class AdminServiceImpl implements AdminService {

	//on utiliser dans cette instruction l'injection de dependance de la classe adminDao
	@Autowired
    private	AdminDao adminDao;
	
	public AdminServiceImpl() {
		
	}
	
	@Override
	public Administrateur getAdmin(String Login, String mp) {
		Administrateur a = new Administrateur();
		a=this.adminDao.findAllBy2Properties("login", Login, "password", mp).get(0); 
		return a;
	}

	@Override
	public void update(Administrateur a) {
		this.adminDao.update(a);
	}

	@Override
	public List<Administrateur> getAllAdmin() {
		return adminDao.findAll();
	}
	public AdminDao getAdminDao() {
		return adminDao;
	}
	public void setAdminDao(AdminDao adminDao) {
		this.adminDao = adminDao;
	}
	@Override
	public List<Administrateur> Rechercheby1Properties(String propertyName1, Object value1) {
		return this.adminDao.findAllBy1Properties(propertyName1, value1);
	}
	@Override
	public List<Administrateur> Rechercheby2Properties(String propertyName1, Object value1, String propertyName2,
			Object value2) {
		return this.adminDao.findAllBy2Properties(propertyName1, value1, propertyName2, value2);
	}

}
