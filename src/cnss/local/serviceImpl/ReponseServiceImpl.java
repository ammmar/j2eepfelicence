package cnss.local.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cnss.local.dao.ReponseDao;
import cnss.local.entity.Reponse;
import cnss.local.service.ReponseService;

/**
 * 
 *la classe service elle est considérer comme 
 *la logique métier dans l'application c'est ta dire les calcul (Bussiness) 
 *
 */
@Service("reponseService")
public class ReponseServiceImpl  implements ReponseService{

	//on utiliser dans cette instruction l'injection de dependance de la classe reponseDao
	@Autowired
	private ReponseDao reponseDao;
	public ReponseServiceImpl() {
		super();
	}
	@Override
	public void ajouterReponse(Reponse r) {
		this.reponseDao.save(r);
		
	}

	public ReponseDao getReponseDao() {
		return reponseDao;
	}

	public void setReponseDao(ReponseDao reponseDao) {
		this.reponseDao = reponseDao;
	}
	
	
	
}
