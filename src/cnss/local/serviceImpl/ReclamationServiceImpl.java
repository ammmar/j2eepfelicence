package cnss.local.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cnss.local.dao.EtatDao;
import cnss.local.dao.ReclamationDao;
import cnss.local.dao.ReponseDao;
import cnss.local.entity.Etatreclamation;
import cnss.local.entity.Reclamation;
import cnss.local.entity.Reponse;
import cnss.local.service.ReclamationService;

/**
 * 
 *la classe service elle est considérer comme 
 *la logique métier dans l'application c'est ta dire les calcul (Bussiness) 
 *
 */
@Service("reclamationService")
public class ReclamationServiceImpl implements ReclamationService {

	//on utiliser dans cette instruction l'injection de dependance de la classe ReclamationDao
	@Autowired
	ReclamationDao reclamationDao; 

	@Autowired
	EtatDao etatDao;
	
	@Autowired
	ReponseDao reponseDao;
	
	public ReclamationServiceImpl() {
	}

	@Override
	public void update(Reclamation r) {
		this.reclamationDao.update(r);
	}
	
	public void updateEtat(Etatreclamation er)
	{
		this.etatDao.update(er);
	}
	@Override
	public void saveEtat(Etatreclamation er)
	{
		this.etatDao.save(er);
	}
	
	@Override
	public Reclamation getReclamationById(long idR) {
		return this.reclamationDao.findById(idR);
	}
	@Override
	public void SaveReponse(Reponse rep) {
		this.reponseDao.save(rep);	
	}

	@Override
	public List<Reclamation> getAllreclamation() {
		return reclamationDao.findAll();
	}
	public ReclamationDao getReclamationDao() {
		return reclamationDao;
	}

	public void setReclamationDao(ReclamationDao reclamationDao) {
		this.reclamationDao = reclamationDao;
	}
		
	public EtatDao getEtatDao() {
		return etatDao;
	}
	public void setEtatDao(EtatDao etatDao) {
		this.etatDao = etatDao;
	}
	
	public ReponseDao getReponseDao() {
		return reponseDao;
	}
	public void setReponseDao(ReponseDao reponseDao) {
		this.reponseDao = reponseDao;
	}
	
	
}
