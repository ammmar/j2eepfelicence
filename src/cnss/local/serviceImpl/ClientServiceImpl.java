package cnss.local.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cnss.local.dao.ClientDao;
import cnss.local.entity.Client;
import cnss.local.service.ClientService;

/**
 * 
 *la classe service elle est considérer comme 
 *la logique métier dans l'application c'est ta dire les calcul (Bussiness) 
 *
 */
@Service("clientService")
public class ClientServiceImpl implements ClientService {
	
	//on utiliser dans cette instruction l'injection de dependance de la classe clientDao
	@Autowired
	private ClientDao clientDao;
	@Override
	public List<Client> getAllClient() {
		return clientDao.findAll();
	}
	@Override
	public void saveOrUpdateClient(Client C) {
		System.out.println("Method saveOrupdate "+C.toString());
		this.clientDao.saveOrUpdate(C);	
	}

	@Override
	public void saveClient(Client C) {
		System.out.println("Method save "+C.toString());
	this.clientDao.save(C);
	}

	@Override
	public void updateClient(Client c) {
		System.out.println("Method update "+c.toString());
		this.clientDao.update(c);	
	}
	@Override
	public List<Client> Rechercheby2Properties(String propertyName1, Object value1, String propertyName2,
			Object value2) {
	 return this.clientDao.findAllBy2Properties(propertyName1, value1, propertyName2, value2);

	}

	@Override
	public boolean ExisteClient(String password, long matricule) {
		return false;
	}
	@Override
	public List<Client> Rechercheby1Properties(String propertyName1, Object value1) {
		return this.clientDao.findAllBy1Properties(propertyName1, value1);
	}

	
}
