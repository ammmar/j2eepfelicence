package cnss.local.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cnss.local.dao.TypeReclamation;
import cnss.local.entity.Typereclamation;
import cnss.local.service.TypeReclamationService;


/**
 * 
 *la classe service elle est considérer comme 
 *la logique métier dans l'application c'est ta dire les calcul (Bussiness) 
 *
 */
@Service("typeReclamationService")
public class TypeReclamationServiceImpl implements TypeReclamationService {

	//on utiliser dans cette instruction l'injection de dependance de la classe typeReclamationDao
	@Autowired
	TypeReclamation typeReclamationDao;

	@Override
	public String AjouterTypeReclamation(Typereclamation tr) {
		boolean b=false;
		int minimum=100;
		int maximum=1000;
		int randomNum=0;
		String etattype="NonExiste";
		List<Typereclamation>Listype;
			if(typeReclamationDao.findAllBy1Properties("description", tr.getDescription()).size()>0)
			{
				etattype="Existe";
			}
		if(!etattype.equals("Existe"))
		{
			randomNum = minimum + (int)(Math.random() * maximum); 
			tr.setIdtype((long)randomNum);
		this.typeReclamationDao.save(tr);
		}
		
		return etattype;
	}

	public void setTypeReclamationDao(TypeReclamation typeReclamationDao) {
		this.typeReclamationDao = typeReclamationDao;
	}
	public TypeReclamation getTypeReclamationDao() {
		return typeReclamationDao;
	}
}
