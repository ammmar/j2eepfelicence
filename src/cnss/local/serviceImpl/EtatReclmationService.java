package cnss.local.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cnss.local.dao.EtatDao;
import cnss.local.entity.Etatreclamation;
import cnss.local.service.EtatService;

/**
 * 
 *la classe service elle est considérer comme 
 *la logique métier dans l'application c'est ta dire les calcul (Bussiness) 
 *
 */
@Service("etatService")
public class EtatReclmationService  implements EtatService{

	//on utiliser dans cette instruction l'injection de dependance de la classe clientDao
	@Autowired
	private EtatDao etatDao;
	@Override
	public List<Etatreclamation> getAllEtat() {
		return this.etatDao.findAll();
	}

	@Override
	public void save(Etatreclamation er) {
		this.etatDao.save(er);	
	}

	@Override
	public void update(Etatreclamation er) {
		this.etatDao.update(er);	
	}

	public EtatDao getEtatDao() {
		return etatDao;
	}

	public void setEtatDao(EtatDao etatDao) {
		this.etatDao = etatDao;
	}
	
}
